/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnalysisPackage;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 *
 * @author mantr
 */
public class Analyzer {

    
//     1)	Our top 3 most popular product sorted from high to low. - Payal
//     2)	Our 3 best customers - Vaishali
//     3)	Our top 3 best sales people - Manogna
//     4)	Our total revenue for the year - All
    
     
    
    public void topThreeBestCustomers() {

        int profit = 0;
        Map<Integer, Order> orderMap = DataStore.getInstance().getOrder();
        Map<Integer, Product> productMap = DataStore.getInstance().getProduct();
        Map<Integer, Integer> profitMap = new HashMap<Integer, Integer>();

        for (Order order : orderMap.values()) {
            int customerID = order.getCustomerId();
            profit = ((order.getItem().getSalesPrice()) - (productMap.get(order.getItem().getProductId()).getMinPrice())) * (order.getItem().getQuantity());

            if (profitMap.containsKey(order.getCustomerId())) {
                profitMap.put(customerID, profitMap.get(customerID) + profit);
            } else {
                profitMap.put(customerID, profit);
            }
        }

        Set<Map.Entry<Integer, Integer>> set = profitMap.entrySet();

        List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(set);

        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {

            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {

                return o2.getValue().compareTo(o1.getValue());
            }
        });
        for (int i = 0; i < 3; i++) {
            System.out.println("Customer ID: " + list.get(i).getKey() + "   Profit Generated: $" + list.get(i).getValue());
        }

    }

    public void topThreeMostPopularProducts() {
    

     
        int popular = 0;

        Map<Integer, Order> orders = DataStore.getInstance().getOrder();
        Map<Integer, Product> ProductMap = DataStore.getInstance().getProduct();
        Map<Integer, Integer> Popularprod = new HashMap<>();
        int ProductID = 0;
        for (Order order : orders.values()) {

            Item item = order.getItem();
            ProductID = item.getProductId();
            popular = ((item.getSalesPrice()) - (ProductMap.get(item.getProductId()).getMinPrice())) * (item.getQuantity());
            if (Popularprod.containsKey(item.getProductId())) {
                Popularprod.put(ProductID, (Popularprod.get(ProductID)) + popular);
            } else {
                Popularprod.put(ProductID, popular);
            }
        }

        List<Map.Entry<Integer, Integer>> prod = new ArrayList<Map.Entry<Integer, Integer>>(Popularprod.entrySet());

        Collections.sort(prod, new Comparator<Map.Entry<Integer, Integer>>() {

            @Override
            public int compare(Map.Entry<Integer, Integer> P1, Map.Entry<Integer, Integer> P2) {
                return (P2.getValue()).compareTo(P1.getValue());
            }

        });
        System.out.println("Popular products");

        for (int i = 0; i < 3; i++) {
            System.out.println("Product ID: " + prod.get(i).getKey() + " " + prod.get(i).getValue());
        }
    }
        public void ThreeBestSalesPeople() {
        int Profit = 0;

        Map<Integer, Order> OrderMap = DataStore.getInstance().getOrder();
        Map<Integer, Product> ProductMap = DataStore.getInstance().getProduct();
        Map<Integer, Integer> ProfitMap = new HashMap<>();
        int SupplierID = 0;
        for (Order o : OrderMap.values()) {

            SupplierID = o.getSupplierId();

            ProductMap.get(o.getItem().getProductId());
            Profit = ((o.getItem().getSalesPrice()) - (ProductMap.get(o.getItem().getProductId()).getMinPrice())) * (o.getItem().getQuantity());
            if (ProfitMap.containsKey(o.getSupplierId())) {
                ProfitMap.put(SupplierID, (ProfitMap.get(SupplierID)) + Profit);
            } else {
                ProfitMap.put(SupplierID, Profit);
            }
        }

        List<Map.Entry<Integer, Integer>> ThreeBestSalesmen = new ArrayList<Map.Entry<Integer, Integer>>(ProfitMap.entrySet());
        Collections.sort(ThreeBestSalesmen, new Comparator<Map.Entry<Integer, Integer>>() {

            @Override
            public int compare(Map.Entry<Integer, Integer> P1, Map.Entry<Integer, Integer> P2) {
                return (P2.getValue()).compareTo(P1.getValue());
            }

        });
        System.out.println("Best Three Salesmen");

        for (int i = 0; i < 3; i++)        
        {
            System.out.println("User ID: " + ThreeBestSalesmen.get(i).getKey() +" Profit Generated: $"+ThreeBestSalesmen.get(i).getValue());
        }
       
    }

     public void TotalRevenue() {
        int Total = 0;
        int Profit = 0;

        Map<Integer, Order> OrderMap = DataStore.getInstance().getOrder();
        Map<Integer, Product> ProductMap = DataStore.getInstance().getProduct();
        Map<Integer, Integer> ProfitMap = new HashMap<>();
        for (Order o : OrderMap.values()) {

            ProductMap.get(o.getItem().getProductId());
            Profit = ((o.getItem().getSalesPrice()) - (ProductMap.get(o.getItem().getProductId()).getMinPrice())) * (o.getItem().getQuantity());
            Total = Total + (Profit);
        }    
            System.out.println("Total Revenue:   $"+Total);
            
           

     }
     
}
