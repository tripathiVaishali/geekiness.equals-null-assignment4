/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import AnalysisPackage.Analyzer;
import AnalysisPackage.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import com.sun.jmx.snmp.BerDecoder;
import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    
    
    
     Analyzer analyze;
    DataReader orderReader;
    DataReader productReader;
    public GateWay() throws IOException
    {
        DataGenerator generator = DataGenerator.getInstance();
       analyze = new Analyzer(); 
        orderReader = new DataReader(generator.getOrderFilePath());
       
        productReader = new DataReader(generator.getProductCataloguePath());
    }
    
    public static void main(String args[]) throws IOException{
        
        GateWay gate = new GateWay();
        gate.readData();
    }
    
      public void readData() throws IOException{
        
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
       
        
          
          
          
        
        String[] orderRow;
       // printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
          //  printRow(orderRow);
            generateCustomer(orderRow);
            generateSalesperson(orderRow);
           Item item= generateItem(orderRow);
            generateOrder(orderRow,item);
            
        }
        System.out.println("_____________________________________________________________");
        
        
        String[] prodRow;
      //  printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
         //   printRow(prodRow);
            generateProduct(prodRow);
        }
        
        
        
        runAnalysis();
        
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    
    private void generateCustomer(String[] row){
        int CustId = Integer.parseInt(row[5]);
        Customer c = new Customer(CustId);
        DataStore.getInstance().getCustomer().put(CustId,c);
    }
    
    private void generateSalesperson(String[] row)
    {
         int SalesId = Integer.parseInt(row[4]);
        SalesPerson sales = new SalesPerson(SalesId);
        DataStore.getInstance().getSalesPerson().put(SalesId,sales);
    }
    
    private Item generateItem(String[] row)
    {
        int productId = Integer.parseInt(row[2]);
        int salesprice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        Item i = new Item(productId,salesprice,quantity);
        return i;
        
//        int commentinguserId = Integer.parseInt(row[4]);
//        int likes = Integer.parseInt(row[3]);
//        int postId = Integer.parseInt(row[1]);
//        String comment = row[5];
//        Comment c = new Comment(commentId,commentinguserId, postId, comment,likes);
//        DataStore.getInstance().getComments().put(commentId,c);
//        Map<Integer,User> users = DataStore.getInstance().getUsers();
//        if(users.containsKey(commentinguserId))
//            users.get(commentinguserId).getComments().add(c);
//        return c;
        
      
    }
    
    private void generateOrder(String[] row ,Item item)
    {
        int orderId = Integer.parseInt(row[0]);
        int suppId = Integer.parseInt(row[4]);
        int custId = Integer.parseInt(row[5]);
         
        Order o = new Order(orderId,suppId,custId,item);
        DataStore.getInstance().getOrder().put(orderId, o);
//       Map<Integer, Order> Orders = DataStore.getInstance().getOrder();
//       if(Orders.containsKey(orderId))
////            posts.get(postId).getComments().add(comment);
//           Orders.get(orderId).getItem().
////        else{
////            Post post = new Post(postId, userId);
////            post.getComments().add(comment);
////            posts.put(postId, post);
////        }
        
        
    }
    
    private void generateProduct(String[] row)
    {
        int prodID = Integer.parseInt(row[0]);
        int min = Integer.parseInt(row[1]);
        int max = Integer.parseInt(row[2]);
        int target = Integer.parseInt(row[3]);
        
        Product p = new Product(prodID,min,max,target);
        DataStore.getInstance().getProduct().put(prodID, p);
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
//    private void generate(String[] row){
//        int commentId = Integer.parseInt(row[0]);
//        int commentinguserId = Integer.parseInt(row[4]);
//        int likes = Integer.parseInt(row[3]);
//        int postId = Integer.parseInt(row[1]);
//        String comment = row[5];
//        Comment c = new Comment(commentId,commentinguserId, postId, comment,likes);
//        DataStore.getInstance().getComments().put(commentId,c);
//        Map<Integer,User> users = DataStore.getInstance().getUsers();
//        if(users.containsKey(commentinguserId))
//            users.get(commentinguserId).getComments().add(c);
//        return c;
//    }
//    
//    private void generatePost(String[] row, Comment comment){
//        int postId = Integer.parseInt(row[1]);
//        int userId = Integer.parseInt(row[2]);
//        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
//        if(posts.containsKey(postId))
//            posts.get(postId).getComments().add(comment);
//        else{
//            Post post = new Post(postId, userId);
//            post.getComments().add(comment);
//            posts.put(postId, post);
//        }
    //}
    
    
    
    void runAnalysis()
    {
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
        System.out.println("\n*************************************************************\nChoose the analysis you wish to run:\n"
                + "1)	Our top 3 most popular product sorted from high to low.\n" +
"2)	Our 3 best customers\n" +
"3)	Our top 3 best sales people\n" +
"4)	Our total revenue for the year\n"+
"5)      Exit"
                + "\n*************************************************************");              
        choice = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Entered Choice: " + choice);
            switch (choice) {

                case 1:
                    analyze.topThreeMostPopularProducts();
                    break;
                case 2:
                    analyze.topThreeBestCustomers();
                    break;
                case 3:
                    analyze.ThreeBestSalesPeople();
                    break;
                case 4:
                    analyze.TotalRevenue();
                    break;
                case 5:
                    break;
                default:
                    break;
        
        }
        }while (choice != 5);
    }
}
        
        
    


